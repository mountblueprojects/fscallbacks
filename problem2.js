const fs = require("fs");
const path = require("path");

function deleteFiles(err) {
  if (err) {
    console.log("Error", err);
  } else {
    fs.readFile(path.join(__dirname, "filenames.txt"), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        console.log("Read filenames.txt");

        const dataArray = data.toString().split("\n");

        for (let index = 0; index < dataArray.length; index++) {
          fs.unlink(path.join(__dirname, dataArray[index]), (err) => {
            if (err) {
              console.log("Error", err);
            } else {
              console.log("File deleted");
            }
          });
        }
      }
    });
  }
}

function sortData(err, fileName, callback) {
  if (err) {
    console.log("Error", err);
  } else {
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        //Data is first split into sentences in an array and then joined after sorting in separate lines
        const newData2 = data
          .toString()
          .split("\n")
          .sort((currentSentence, nextSentence) => {
            return currentSentence.localeCompare(nextSentence);
          })
          .join("\n");

        const fileName3 = "SortSentences.txt";

        fs.writeFile(path.join(__dirname, fileName3), newData2, (err) => {
          if (err) {
            console.log("Error", err);
          } else {
            console.log("Created Sorted file from Lowercase file");

            fs.appendFile(
              path.join(__dirname, "filenames.txt"),
              "\n" + fileName3,
              (err) => {
                if (err) {
                  console.log("Error", err);
                } else {
                  console.log("Appended sorted filename");

                  callback(null);
                }
              }
            );
          }
        });
      }
    });
  }
}

function lowerCaseWrite(err, fileName, callback) {
  if (err) {
    console.log("Error", err);
  } else {
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
      if (err) {
        console.log("Error", err);
      } else {
        //Splitting the sentences into an array
        const newData = data.toString().toLowerCase().split(".");
        const fileName2 = "LowercaseSentences.txt";

        //Data is joined with newline to obtain each sentence in each line
        fs.writeFile(
          path.join(__dirname, fileName2),
          newData.join("\n"),
          (err) => {
            if (err) {
              console.log("Error", err);
            } else {
              console.log("Created lowercase file from Uppercase file");

              //Filename is stored as nextLine in filenames.txt
              fs.appendFile(
                path.join(__dirname, "filenames.txt"),
                "\n" + fileName2,
                (err) => {
                  if (err) {
                    console.log("Error", err);
                  } else {
                    console.log("Appended lowercase filename");

                    callback(null, fileName2, deleteFiles);
                  }
                }
              );
            }
          }
        );
      }
    });
  }
}

function upperCaseWrite(err, data, callback) {
  if (err) {
    console.log("Error", err);
  } else {
    const fileName1 = "Uppercase.txt";

    fs.writeFile(
      path.join(__dirname, fileName1),
      data.toString().toUpperCase(),
      (err) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("Created Uppercase File");

          fs.writeFile(
            path.join(__dirname, "filenames.txt"),
            fileName1,
            (err) => {
              if (err) {
                console.log("Error", err);
              } else {
                console.log("Stored Uppercase Filename");

                callback(null, fileName1, sortData);
              }
            }
          );
        }
      }
    );
  }
}

function readFile(fileName, callback) {
  fs.readFile(path.join(__dirname, fileName), (err, data) => {
    if (err) {
      console.log("Error", err);
    } else {
      console.log("File read");

      callback(null, data, lowerCaseWrite);
    }
  });
}

module.exports.readFile = readFile;
module.exports.upperCaseWrite = upperCaseWrite;
module.exports.lowerCaseWrite = lowerCaseWrite;
module.exports.sortData = sortData;
module.exports.deleteFiles = deleteFiles;
