const problem2 = require("../problem2");

const readFile = problem2.readFile;
const upperCaseWrite = problem2.upperCaseWrite;
const lowerCaseWrite = problem2.lowerCaseWrite;
const sortData = problem2.sortData;
const deleteFiles = problem2.deleteFiles;

readFile("lipsum.txt", upperCaseWrite);
