const fs = require("fs");
const path = require("path");

function deleteFile(err, filePath) {
  if (err) {
    console.log("Error", err);
  } else {
    fs.unlink(filePath, (err) => {
      if (err) {
        console.log("Error", err);
      } else {
        console.log("File deleted");
      }
    });
  }
}

function createFile(err, directoryName, callback) {
  if (err) {
    console.log("Error", err);
  } else {
    const files = [
      "file1.json",
      "file2.json",
      "file3.json",
      "file4.json",
      "file5.json",
    ];

    for (let index = 0; index < files.length; index++) {
      fs.writeFile(
        path.join(__dirname, directoryName, files[index]),
        "Random",
        (err) => {
          if (err) {
            console.log("Error", err);
          } else {
            console.log("File created");

            callback(null, path.join(__dirname, directoryName, files[index]));
          }
        }
      );
    }
  }
}

function createDirectory(directoryName, callback) {
  fs.mkdir(path.join(__dirname, directoryName), (err) => {
    if (err) {
      console.log("Error", err);
    }
    console.log("Folder created");

    callback(null, directoryName, deleteFile);
  });
}

module.exports.createDirectory = createDirectory;
module.exports.createFile = createFile;
module.exports.deleteFile = deleteFile;
